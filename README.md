Exercise 06: Repulsive Particles on the GPU
===========================================

Due: Tuesday, November 21, 2017 at 11:55 p.m.

10 points + 2 extra credit points available

Submission as `ex06.pdf` on T-Square

This assignment is like a single `notebook` entry of the type used in Project 2, but applied to the familiar problem of interacting particles.

---

1. Log into [bridges](../../notes/bridges/) and verify:

    - That you can load the petsc module with `module use /home/tisaac/opt/modulefiles` and `module load petsc/cse6230-double`
    - That you can load the `cuda` module
    - That you can compile `ex06` from `ex06.cu`
    - That you can submit `slurm-ex06-k80.sh`

2. (3 points) Assess the performance of `compute_forces`.  In your report,
   include the script that you used to assess the performance
   and describe ways that the initial implementation violates the performance
   principles for GPUs that we've discussed.

    - +2 Extra credit: use `nvprof` / Nvidia Visual Profiler to corroborate your answer.  You may use the automatic analysis tools.

3. (3 points) Briefly describe the changes that you will make to `compute_forces` and why you think they will improve performance.
  
4. (4 points) Implement your optimization of `compute_forces`.  In your report,
   include a listing of just the `compute_forces` kernel.  Your changes may
   also include changes to the thread block layour of the kernel launch.
   Implementations that receive full credit should use at least two of the following techniques:

    - Instruction level parallelism
    - Thread block shared memory
    - Avoidance of branch divergence
    - Coalesced memory access
    - Aligned memory access

5. Assess the performance of your updated program with the same script used before

6. Create a pdf `ex06.pdf` with:

    - Your performance assessment script.
    - The output of your performance script applied to the initial version of the code.
    - Your assessment of the initial performance.
    - (Extra credit: `nvprof` / `nvvp` corroboration).
    - Your descriptions of the planned changes.
    - Listing of `compute_forces`.
    - The output of your initial performance script applied to the final version of the code.

